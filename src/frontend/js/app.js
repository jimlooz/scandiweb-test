function router(routes)
{
    this.routes = routes;

    $(window).on('hashchange', () => {
        this.check(window.location.hash);
    });

    this.init = function ()
    {
        this.check(window.location.hash);
    }

    this.check = function(hash)
    {
        let page = null;

        page = (this.routes).find(route => route == hash);
        console.log(page);
        if(page == null)
            page = 'list';

        return this.render(page);
    }

    this.render = function(page)
    {
        $('#app').load(`/src/frontend/pages/${page.replace('#', '')}.html`);
    }

}

let routes = ['#list', '#add'];

let spaRouter = new router(routes);

spaRouter.init();