$(document).ready(() => {
    $('#productType').change(function() {
        switch (this.value) {
            case '0':
                $('#attributes').html(`
                    <label for="size">Size</label>
                    <input type="number" step="0.01" class="form-control" id="size" name="size" required>
                    <p>
                        Pleas provide Size in MB.

                    </p>
                `);
                break;
            case '1':
                $('#attributes').html(`
                    <label for="weight">Weight</label>
                    <input type="number" step="0.01" class="form-control" id="weight" name="weight" required>
                    <p>
                        Pleas provide Weight in Kg.

                    </p>
                `);
                break;

            case '2':
                $('#attributes').html(`
                    <label for="height">Height</label>
                    <input type="number" step="0.01" class="form-control" id="height" name="height" required>
                    <label for="width">Width</label>
                    <input type="number" step="0.01" class="form-control" id="width" name="width" required>
                    <label for="length">Length</label>
                    <input type="number" step="0.01" class="form-control" id="length" name="length" required>
                    <p>
                        Pleas provide dimensions in cm.
                    </p>
                `);
                break;
        }
    });

    $('form').submit(function(e) 
    {
        e.preventDefault();

        let inputs = {};
        $(this).find(':input').each(function() {
            inputs[$(this).attr("name")] = $(this).val();
        });

        $.post('/products/add', inputs, function() {    
            window.location.href="/#list";
        });
    });
});